#!/usr/bin/python
# -*-coding:utf-8 -*

""" By the power of Sid Drenan, Undercity shalt databend >:))) """

from pywmo import Wmo
import random

random.seed()

UC_PATH = "D:\\Programmes\\Mod WoW\\Work\\" \
          "World\\wmo\\Lorderon\\Undercity\\Undercity.wmo"
SID = 5



def get_rand(scale):
    return (random.random() * scale) - (scale / 2)


 
undercity = Wmo(UC_PATH)

for ig, group in enumerate(undercity.groups):
    
    # Deformation
    for iv, v in enumerate(group.vertices):
        undercity.groups[ig].vertices[iv] = (
            v[0] + get_rand(SID),
            v[1] + get_rand(SID),
            v[2] + get_rand(SID) )
        
    # Randomize vertex colors
    try:
        for ic in range(len(group.vertex_colors)):
            undercity.groups[ig].vertex_colors[ic] = (
                int(random.randint(0, 255)),
                int(random.randint(0, 255)),
                int(random.randint(0, 255)),
                int(random.randint(0, 255)) )
    except AttributeError:
        pass
        
        
undercity.write_file("..\\tests\\test.wmo")
