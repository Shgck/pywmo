#!/usr/bin/python
# -*-coding:utf-8 -*

""" Applies random vertex shading for every WMO in the WMO_DIR directory.
    *humming* Do you believe in magic ?... """

import os
import re
import random
from pywmo import Wmo

random.seed()



# Your directory with all your WMO.
# Beware, they will be replaced by the new ones.
WMO_DIR = "D:\\Programmes\\Mod WoW\\ALL LK WMO - Copie"

group_file_regex = re.compile(".*_[0-9]{3}.wmo$")

def is_group_file(filename):
    return (group_file_regex.match(filename) != None)



for dir_name, dir_names, file_names in os.walk(WMO_DIR):
    # dirname : courant
    # dirnames : r�pertoires dans le rep. courant
    # filenames : fichiers dans " " " 
         
    for file_name in file_names:
        if not is_group_file(file_name):
            print("{} ...".format(file_name))
            model = Wmo(os.path.join(dir_name, file_name))
            for ig, group in enumerate(model.groups):
                try:
                    for ic in range(len(group.vertex_colors)):
                        model.groups[ig].vertex_colors[ic] = (
                            int(random.randint(0, 255)),
                            int(random.randint(0, 255)),
                            int(random.randint(0, 255)),
                            int(random.randint(0, 255)) )
                except AttributeError:
                    pass
            model.write_file(os.path.join(dir_name, file_name))
            
