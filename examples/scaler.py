#!/usr/bin/python
# -*-coding:utf-8 -*

""" Scale a WMO with an argument from the command line.

    Usage : scaler model.wmo <new_scale>
    new_scale being a float.
    
    An argument of 0 will reduce all your vertices to 0,0,0 and create
    some kind of black hole simulation... I guess.
    
    An argument of 1 will let the model as such.
    
    This is just a sample code for PyWMO and lacks actual work.
    It will erase your liquid information, your portals and will set the groups
    to outdoor ; there is a problem with portal scaling.
"""

import sys
from pywmo import Wmo



def scale_vertex(v, scale):
    return (v[0] * scale, v[1] * scale, v[2] * scale)
def scale_u16_vertex(v, scale):
    return (int(v[0] * scale), int(v[1] * scale), int(v[2] * scale))



print("Loading...")

try:
    model = Wmo(sys.argv[1])
    scale = float(sys.argv[2])
except IndexError:
    print("Usage : scaler model.wmo <new_scale>")
    exit()



print("Processing...")

#### On root

# Bounding box needs to be scaled
model.bbox1 = (scale_vertex(model.bbox1, scale))
model.bbox2 = (scale_vertex(model.bbox2, scale))

for ig, group_info in enumerate(model.group_infos):
    f = group_info["flags"]
    # DEBUG : remove indoor, set outdoor 
    if f & 0x2000:
        f -= 0x2000
        f |= 0x8
    model.group_infos[ig]["flags"] = f
    model.group_infos[ig]["bbox1"] = scale_vertex(group_info["bbox1"], scale)
    model.group_infos[ig]["bbox2"] = scale_vertex(group_info["bbox2"], scale)

# If the model has portals, they need to be scaled too.
#for ip, portal in enumerate(model.portal_vertices):
#    model.portal_vertices[ip] = ( scale_vertex(portal[0], scale)
#                                , scale_vertex(portal[1], scale)
#                                , scale_vertex(portal[2], scale)
#                                , scale_vertex(portal[3], scale) )
# Necessary ?
#for ip, portal in enumerate(model.portal_infos):
#    model.portal_infos[ip]["vector"] = scale_vertex(portal["vector"], scale) 
    
# Scaling doodad position and their actual scale
for i, doodad in enumerate(model.doodad_defs):
    model.doodad_defs[i]["position"] = scale_vertex(doodad["position"], scale)
    model.doodad_defs[i]["scale"] = doodad["scale"] * scale 

#### On group

for ig, group in enumerate(model.groups):
    
    f = model.groups[ig].flags
    # DEBUG : delete liquid flag, see below for exp
    if f & 0x1000:
        f -= 0x1000
    # DEBUG : remove indoor, set outdoor 
    if f & 0x2000:
        f -= 0x2000
        f |= 0x8
    model.groups[ig].flags = f

    model.groups[ig].num_portals = 0
    
    # Scale the group bounding box
    model.groups[ig].bbox1 = scale_vertex(group.bbox1, scale)
    model.groups[ig].bbox2 = scale_vertex(group.bbox2, scale)
    
    # Scale all mesh vertices
    for iv, v in enumerate(group.vertices):
        model.groups[ig].vertices[iv] = scale_vertex(v, scale)
        
    # Scale batches bounding box (is it necessary ? are they really bboxes ?)
    for ib, batch in enumerate(group.batches):
        model.groups[ig].batches[ib]["bbox1"] = \
            scale_u16_vertex(batch["bbox1"], scale) 
        model.groups[ig].batches[ib]["bbox2"] = \
            scale_u16_vertex(batch["bbox2"], scale)
    
    # Scaling BSP tree fDists if any...    
    try:
        for i, node in enumerate(group.bsp_nodes):
            if node["type"] != "leaf":
                model.groups[ig].bsp_nodes[i]["f_dist"] = \
                    node["f_dist"] * scale
    except AttributeError:
        pass
    
    # Scaling liquids if any...
    # ok for some reason it doesn't work ; the liquids get well placed
    # but are still at their original size.
    # I don't know how to scale a liquid tile nor if it's even possible.
#     try:
#         model.groups[ig].liquids["base"] = \
#             scale_vertex(model.groups[ig].liquids["base"], scale)
#         for i, vec2f in enumerate(model.groups[ig].liquids["height_map"]):
#             model.groups[ig].liquids["height_map"][i] = \
#                 (vec2f[0], vec2f[1] * scale)
#     except AttributeError:
#         pass
    # Deleting liquids
    try:
        del model.groups[ig].liquids
    except AttributeError:
        pass
        

print("Saving...")

model.write_file("test.wmo")


