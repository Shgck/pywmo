#!/usr/bin/python
# -*-coding:utf-8 -*

""" Quick command line tool to set indoor / outdoor flags to WMO groups. """

import os
import sys
from pywmo import Wmo
from _dummy_thread import exit



OUTDOOR_FLAG = 0x8
INDOOR_FLAG = 0x2000

_HELP_TEXT = """Available commands

\t+ help : display this help
\t+ names : display available group names
\t+ q : quit
\t+ save : save file. A path can be given as argument,
\t         else original file is overwritten.
\t+ set in : set specified groups indoor.
\t           ex : set in 0 1 2 5 6
\t           if no group specified, applies to every group.
\t+ set out : same for outdoor

"""



def get_command():
    sys.stdout.write(">")
    sys.stdout.flush()
    return sys.stdin.readline().split()



def set_outdoor(model, group_id):
    # Root
    f = model.group_infos[group_id]["flags"]
    if f & INDOOR_FLAG:
        f -= INDOOR_FLAG
    f |= OUTDOOR_FLAG
    model.group_infos[group_id]["flags"] = f
    
    # Group
    f = model.groups[group_id].flags
    if f & INDOOR_FLAG:
        f -= INDOOR_FLAG
    f |= OUTDOOR_FLAG
    model.groups[group_id].flags = f
    
def set_indoor(model, group_id):
    # Root
    f = model.group_infos[group_id]["flags"]
    if f & OUTDOOR_FLAG:
        f -= OUTDOOR_FLAG
    f |= INDOOR_FLAG
    model.group_infos[group_id]["flags"] = f
    
    # Group
    f = model.groups[group_id].flags
    if f & OUTDOOR_FLAG:
        f -= OUTDOOR_FLAG
    f |= INDOOR_FLAG
    model.groups[group_id].flags = f

def display_names(model):
    for i, info in enumerate(model.group_infos):
        if info["name_index"] != -1:
            for offset, name in model.group_names:
                if offset == info["name_index"]:
                    print("{} : {}".format(i, name))
                    break
        else:
            print("{} : <unk>".format(i))



def main():
    try:
        model = Wmo(sys.argv[1])
    except IndexError:
        print("Usage : rein_raus model.wmo")
        sys.exit()

    print(_HELP_TEXT)
    
    while True:
        command = get_command()
        
        if not command:
            continue
        
        if command[0] == "help":
            print(_HELP_TEXT)

        if command[0] == "names":
            display_names(model)

        if command[0] == "q" or command[0] == "quit":
            break
        
        if command[0] == "save":
            print("Saving...")
            if len(command) > 1:
                model.write_file(command[1])
            else:
                model.write_file(sys.argv[1])
        
        if command[0] == "set" and len(command) >= 2:
            if command[1] == "in":
                if len(command) == 2:
                    for i in range(model.num_groups):
                        set_indoor(model, i)
                else:
                    for i in command[2:]:
                        set_indoor(model, int(i))
            elif command[1] == "out":
                if len(command) == 2:
                    for i in range(model.num_groups):
                        set_outdoor(model, i)
                else:
                    for i in command[2:]:
                        set_outdoor(model, int(i))
            print("Done.")
        

if __name__ == "__main__":
    main()
    